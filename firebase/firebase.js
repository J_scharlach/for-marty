import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyDWFMEnL9ttp8d7P0n-LvOH9WnubQPLmsY",
    authDomain: "marty-d2d46.firebaseapp.com",
    projectId: "marty-d2d46",
    storageBucket: "marty-d2d46.appspot.com",
    messagingSenderId: "438323166553",
    appId: "1:438323166553:web:0e61f80ba7330925c06048",
    measurementId: "G-4LYZTV2CWR"
  };
  
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  export default firebase;