import React from 'react'
import firebase from '../firebase/firebase'
import styles from '../styles/Home.module.css'

export default function landing() {

    const [names, setName] = React.useState([])

    React.useEffect(() => {
        const fetchData = async () => {
            const db = firebase.firestore()
            const data = await db.collection("test").get()
            setName(data.docs.map(doc => doc.data()))
        }
        fetchData()
    }, [])


    return (
        <div>
            <h1>hello to me</h1>
            <ul>
                {names.map(name => (
                    <li key={name.name} className={styles.name}>{name.name}</li>
                ))}
            </ul>
        </div>
    );
}
